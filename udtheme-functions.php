<?php
/**
  * Force IE Edge
  * Description: Add an X-UA-Compatible header to WordPress
  * Author: Christopher Davis
  * Author URI: http://christopherdavis.me
  * License: GPL2
  * Copyright 2012 Christopher Davis
*/
add_filter('wp_headers', 'ud_ie_xua');
function ud_ie_xua($headers){
    $headers['X-UA-Compatible'] = 'IE=edge,chrome=1';
    return $headers;
}
/**
  * UDTheme Custom Functions
  * Enqueued scripts and styles and assorted actions and filters.
  * http://justintadlock.com/archives/2011/02/02/creating-a-custom-functions-plugin-for-end-users
  * loads conditionally for IE stylesheets
  * http://wordpress.stackexchange.com/questions/48581/enqueue-different-stylesheets-using-ie-conditionals
*/
add_action ('wp_enqueue_scripts','ud_ie_style_sheets');
function ud_ie_style_sheets () {
    global $is_IE; // called from WordPress API
    if($is_IE ) :
      wp_register_style( 'ud-ie', plugins_url( '/public/css/ie.css', __FILE__ ), array(), 3 );
      wp_enqueue_style( 'ud-ie' );
    endif;
    wp_register_script('detection',  plugins_url( '/public/js/detection.js', __FILE__ ), '1.0');
    wp_enqueue_script('detection');
}

/**
  * Adds a default UD fav icon to WordPress
  * check if function exists to avoid conflict with Divi Theme 2/28/14 CL
*/
add_action('wp_head', 'ud_favicon');
function ud_favicon() {
    $favicon_url = MakeItSoNo1::$htmlDir. '/public/img/icons/favicon32.ico';
    $appleicon_url = MakeItSoNo1::$htmlDir. '/public/img/icons/apple-touch-icon.png';
    echo '<link rel="shortcut icon" type="image/x-icon" href="' . $favicon_url . '">' . "\n";
    echo '<link rel="apple-touch-icon" href="' . $appleicon_url . '">' . "\n";
}

/**
  * Check if jQuery is loaded. If not, load it
*/
if( !wp_script_is( 'jquery' ) ) :
wp_enqueue_script( 'jquery' );
endif;

/**
   * Properly Enqueuing Google Fonts in WordPress
   * http://www.wpbeginner.com/wp-themes/how-add-google-web-fonts-wordpress-themes/
*/
add_action('wp_print_styles', 'udtheme_add_google_fonts');
function udtheme_add_google_fonts() {
  wp_register_style('udtheme-googleFonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300');
  wp_enqueue_style( 'udtheme-googleFonts');
}

/**
  * Adds css and js for the main functionality of the plugin
*/
add_action( 'admin_enqueue_scripts', 'ud_change_adminbar_css' );
if (!is_admin()) :
    add_action( 'wp_enqueue_scripts', 'ud_change_adminbar_css' );
endif;

function ud_change_adminbar_css() {
  wp_register_style( 'add-admin-stylesheet', plugins_url( '/admin/css/udt-admin.css', __FILE__ ), array(), 3 );
  wp_enqueue_style( 'add-admin-stylesheet' );
  wp_enqueue_script("functions",  plugins_url( '/admin/js/udt-admin.js', __FILE__ ), array('jquery'), '3.0');
  wp_enqueue_script('jquery-ui-tabs');
}

/**
  * Adjust the css styles and colors depending on white or blue.
*/
function udtheme_custom_colors() {
    $seal_image = 'icons_social_footer_';
    $ud_blue = '#002663';
    $lt_blue = '#8CC8FF';
    $md_blue = '#00539F';
    $md_blue_hover = '#014C91';
    $dk_blue = '#004381';
    $md_yellow = '#FFB41B';
    $dk_yellow = '#EFA000';
    $ud_orange = '#EF8200';
    $ud_yellow = '#FFD200';
    $ud_white = '#FFF';
    $lt_gray = '#F2F2F2';
    $theme = wp_get_theme(); // gets the current theme
?>

<style type="text/css">
<?php
/**
 * http://wordpress.stackexchange.com/questions/131814/if-current-user-is-admin-or-editor
 * set wpadminbar to position fixed to work with UD Theme 8/29/15 CL
 */
$user = wp_get_current_user();
$allowed_roles = array('editor', 'administrator', 'author');
 if( array_intersect($allowed_roles, $user->roles ) ) {  ?>
  @media only screen and (max-width:48em) {#wpadminbar {position:fixed !important;}}
<?php
}
?>
<?php
        $udel_header = get_option('udel_theme_module_option');
        if (  !$udel_header || $udel_header == 'blankHeader' ):
?>
      #udbrand_header {
        display:none !important;
      }
<?php
      endif;

/**
 * Theme overrides
*/
  if ( ( 'Matheson' == $theme->name || 'Matheson' == $theme->parent_theme ) ):
?>
      .image-anchor {
        display:inline
      }
<?php
  endif; // end Matheson Theme check

  if ( ( 'Divi' == $theme->name || 'Divi' == $theme->parent_theme ) ):
?>
      #top-header {
        z-index:4999 !important;
      }
      body.admin-bar.et_fixed_nav #main-header, body.admin-bar.et_fixed_nav #top-header {
        top:0 !important;
      }
      .et_fixed_nav #main-header, .et_fixed_nav #top-header {
          position: relative !important;
      }
      .no-rgba .et_fixed_nav #main-header, .no-rgba .et_fixed_nav #top-header {
          position: fixed !important;
      }
      .et_fixed_nav.et_show_nav.et_secondary_nav_enabled.et_header_style_centered #page-container {
          margin-top: 0 !important;
          padding-top: 0 !important;
      }
<?php
  endif; // end Divi Theme check
?>
<?php
  if ('Tracks' == $theme->name || 'Tracks' == $theme->parent_theme):
?>
      #udbrand_footer {
          z-index:-1;
          background:<?php echo $ud_white; ?>;
      }
<?php
  endif; // end Tracks Theme check
?>
 <?php
// Force primary nav to position relative to fix ud header not showing
  if ('Aaron' == $theme->name || 'Aaron' == $theme->parent_theme):
?>
    #site-navigation {
      position: relative;
    }
<?php
  endif; // end Aaron Theme check
?>

<?php
  if ('Boardwalk' == $theme->name || 'Boardwalk' == $theme->parent_theme) :
?>
       .archive .site-footer, .blog .site-footer, .site-header, .admin-bar .site-header {
          position: relative !important;
          top:0 !important;
        }
<?php
  endif; // end Boardwalk Theme check
?>
<?php
  if ('Radiate' == $theme->name || 'Radiate' == $theme->parent_theme) :
?>
    .header-wrap{
      position: relative !important;
      top:0 !important;
    }
    #udbrand_header {
      z-index:9999 !important;
    }
<?php
  endif;// end Radiate Theme check
?>
<?php
  if ('Twenty Fourteen' == $theme->name || 'Twenty Fourteen' == $theme->parent_theme) :
?>
    .masthead-fixed .site-header {
    position: relative !important;
    top: 0 !important;
}
<?php
  endif; // end Twenty Fourteen Theme check
?>


/**
 * Footer css conditions based on footer choice of white or blue.
*/
<?php
    if ( get_option('udel_footer') != 'blankFooter' ):
              if ('Tracks' == $theme->name || 'Tracks' == $theme->parent_theme):
?>
        #udbrand_footer {
            z-index:-1;
            background:<?php echo $ud_white; ?>;
        }

<?php
      endif; // end Tracks Theme check
$udel_footer_color_option = get_option('udel_footer_color_option');
if ( $udel_footer_color_option == 'Blue' || $udel_footer_color_option != 'White' ):
  ?>
span[data-icon="twitter"]:after,
span[data-icon="facebook"]:after,
span[data-icon="instagram"]:after,
span[data-icon="youtube"]:after,
span[data-icon="pintrest"]:after,
span[data-icon="linkedin"]:after{
        background: url("<?php echo plugins_url('public/img/icons/'.$seal_image.'blue.png', __FILE__);?>") no-repeat;
}
<?php
      endif; // end blue
    endif; // end blockHeader
    if ( $udel_footer_color_option == 'White' ):
?>
        span[data-icon="twitter"]:after,
span[data-icon="facebook"]:after,
span[data-icon="instagram"]:after,
span[data-icon="youtube"]:after,
span[data-icon="pintrest"]:after,
span[data-icon="linkedin"]:after{
  background: url("<?php echo plugins_url('public/img/icons/'.$seal_image.'white.png', __FILE__);?>") no-repeat;
}
<?php
    endif; // end white
?>
</style>

<?php
}
add_action( 'wp_print_scripts', 'udtheme_custom_colors' );
?>

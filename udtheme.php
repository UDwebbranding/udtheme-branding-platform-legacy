<?php
/**
  * Plugin Name:       UDTheme Branding Platform
  * Plugin URI:        https://bitbucket.org/UDwebbranding/udtheme-branding-plugin-legacy
  * Description:       Displays University of Delaware branded header and footer within Wordpress themes.
  *                    Only to be used on official University of Delaware department sites in accordance
  *                    with CPA guidelines.  Based off the udelizer plugin created by John F. Hall
  * Version:           1.5.0
  * Author:            Christopher Leonard - University of Delaware | IT CS&S
  * Author URI:        http://it.udel.edu
  * License:           GPL-3.0
  * License URI:       https://bitbucket.org/UDwebbranding/udtheme-branding-plugin-legacy/license.txt
  * Text Domain:       udtheme
  * Domain Path:
*/

/**
 * http://wordpress.stackexchange.com/questions/27850/deactivate-plugin-upon-deactivation-of-another-plugin
 * Deactivate plugin upon deactivation of another plugin
 */

// register_deactivation_hook(__FILE__,'udtheme');
// function udtheme(){
//    $dependent_udtheme = plugins_url( '/udtheme/udtheme.php' );
//    if( is_plugin_active($dependent_udtheme) ) :
//     add_action('update_option_active_plugins', 'ud_deactivate_udbrand');
//    endif;
//  }

//  function ud_deactivate_udbrand(){
//   $dependent_udbrand = plugins_url( '/ud-branding/ud-branding.php' );
//   deactivate_plugins($dependent_udbrand);
//  }

require_once __DIR__ . '/include/constants.php';

$config['baseUrl']=plugin_dir_url(__FILE__);
MakeItSoNo1::loadConfig($config);
require_once ABSPATH . 'wp-admin/includes/plugin.php';
require_once __DIR__ . '/udtheme-functions.php';
$DEBUG=false;

function udtheme_init( $module_choice ){
  global $current_blog;

  if ( $module_choice == null || $module_choice == ''):
    $module_choice = MakeItSoNo1::$udTheme;
  endif;
  $udel_look = templateFactory::getTemplate($module_choice, 'Sites at UD', 'University of Delaware', 'udel', 'Sites at UD');
  return $udel_look;
}

function udtheme_header(){
  global $current_blog;

  $udt_custom_header_text = get_option('udt_custom_header_text');
  $module_choice = get_option('udel_theme_module_option');
  $udt_custom_header_value = get_option('udt_custom_header_value');
  $udel_look = udtheme_init( $module_choice, $udt_custom_header_value, $udt_custom_header_text);
  $udel_look=udtheme_init($udt_custom_header_text);
  if ( udtheme_non_themed_page() ):
    return;
  endif;

  print ($udel_look->html_ud_css());
  print ($udel_look->html_ud_js());
  print ($udel_look->html_ud_footer_js());
}

function udtheme_footer(){
  global $current_blog;
  $udel_footer = get_option('udel_footer');
  if ( !$udel_footer || $udel_footer == 'blankFooter' ) return;
  $udel_look=udtheme_init(get_option('udel_footer'));
  print($udel_look->html_ud_footer());
}

function udtheme_include($template) {
  ob_start();
  return $template;
}

function udtheme_non_themed_page() {
  if ( strstr($_SERVER['QUERY_STRING'], "visual-editor") ||
       strstr($_SERVER['QUERY_STRING'], "ve-iframe")  ||
       strstr($_SERVER['QUERY_STRING'], "wp-admin") ||
       strstr($_SERVER['REQUEST_URI'], "wp-admin")  ||
       strstr($_SERVER['REQUEST_URI'], "wp-login")
      ):
    return true;
  endif;
  return false;
}

/**
 *    Some functions adapted from workaround in http://core.trac.wordpress.org/ticket/12563
 *      (hack for sticking code right after body tag)
 */
function udtheme_shutdown() {
  global $current_blog;
  $udt_custom_header_text = get_option('udt_custom_header_text');
  $module_choice = get_option('udel_theme_module_option');
  $udt_custom_header_value = get_option('udt_custom_header_value');
  if ( udtheme_non_themed_page() ) { return; }
  $udel_look = udtheme_init( $module_choice, $udt_custom_header_value, $udt_custom_header_text );
  //$udel_look=udtheme_init(get_option('udel_theme_module_option'));
  $insert =  $udel_look->html_ud_header();
  $content = ob_get_clean();

  if ( wp_get_theme('Divi') != false ) :
    $content = preg_replace('#<body([^>]*)>#i',"<body$1>{$insert}",$content);
  endif;
  echo $content;
}

function udtheme_admin_sidebar() {
  add_menu_page( 'udtheme', 'UDTheme', 'manage_options', 'udtheme', 'udtheme_options', plugins_url( '/admin/img/udtheme_sidebar_logo.png', __FILE__ ) );
}

function udtheme_options() {
  global $current_blog;
 // global $udel_theme_module_option;
  //$udt_custom_header_text = $_POST[$value['option']];
   // global $udel_theme_module_option;

   // $post_header_module = $_POST['udel_theme_module_option'];
   // $post_header_module_custom = $_POST['udt_custom_header_value'];
   // $post_udt_custom_header_text = $_POST['udt_custom_header_text'];
   //  $post_footer = $_POST['udel_footer'];
   // $post_footer_color = $_POST['udel_footer_color_option'];

  $udel_message[] = null;

  // update_option('udel_theme_module_option', $_POST['udel_theme_module_option']);
  //   update_option('udt_custom_header_value', $_POST['udt_custom_header_value']);

  // if ( isset( $_POST['udt_custom_header_value'] ) ){
  //     update_option('udt_custom_header_value', $_POST['udt_custom_header_value']);
  //   }
  //   if ( isset( $_POST['udt_custom_header_text'] )  ){
  //     update_option('udt_custom_header_text', $_POST['udt_custom_header_text']);
  //   }

  // if ( isset( $_POST['udel_footer'] ) && ( $_POST['udel_footer'] == 'blank' || $_POST['udel_footer'] == 'blue' || $_POST['udel_footer'] == 'white') ){
  //     update_option('udel_footer', $_POST['udel_footer']);
  //   }





    if ( isset( $_POST['udel_theme_module_option'] ) ) :
      update_option('udel_theme_module_option', $_POST['udel_theme_module_option']);
      update_option('udt_custom_header_value', $_POST['udt_custom_header_value']);
      update_option('udt_custom_header_text', $_POST['udt_custom_header_text']);
    endif;
    if ( isset($_POST['udel_footer_color_option'] ) ) :
      update_option('udel_footer_color_option',$_POST['udel_footer_color_option']);
    endif;
    if ( isset( $_POST['udel_footer'] ) ):
      update_option('udel_footer', $_POST['udel_footer']);
    endif;



  $udel_theme_module_option = get_option('udel_theme_module_option');
  $udt_custom_header_text = get_option('udt_custom_header_text');
  $udt_custom_header_value = get_option('udt_custom_header_value');

  $udel_footer = get_option('udel_footer');
  $udel_footer_color_option = get_option('udel_footer_color_option');
  $dir = plugins_url();

/**
  * layout variables 9/24/14 CL
*/

  $menu_text_head = 'Header Options';
  $menu_text_foot = 'Footer Options';
?>
<p class="notify"></p>
<p class="notify_header_off"></p>
<div id="ud-wrap">
  <div id="panel-wrap">
    <form id="ud_form" method="post" enctype="multipart/form-data">
      <input type="hidden" name="udtheme-update" value="true">
      <div class="row tabs clearfix">
        <div class="blue-sidebar four columns">
        <div class="branddiv">
          <img class="aligncenter" width="230" height="60" src="<?php echo  plugins_url( '/admin/img/udtheme-top-logo.png', __FILE__ ); ?>" alt="UDTheme Branding Platform Plugin">
        </div>
          <ul id="side_menu" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
            <li class="uui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-left-1"><?php echo   $menu_text_head; ?></a></li>
            <li class="ui-state-default ui-corner-top"><a href="#tabs-left-2"><?php echo   $menu_text_foot; ?></a></li>
          </ul>
          <p>The UDTheme Platform Branding plugin is a CPA compliant standard providing the option to add a responsive University of Delaware branded header and footer in white or blue. </p>
          <p>Contact options include standard or custom department information.</p>
          <p><strong> Only official UD department pages may use this plugin.</strong></p>
        </div>
<div id="ud_override" class="grey-main eight columns">

          <?php
                  if ( count($udel_message) >1 ):
                    foreach ( $udel_message as $msg ){
            ?>
              <div class="udel_message"><?php echo $msg; ?></div>
            <?php
                    }
                  endif;
            ?>
          <div id="tabs-left-1" data-tab="0" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
            <h2><?php echo $menu_text_head ?></h2>
            <!-- header main content -->
            <div class="tab-header twelve columns">
                <h3>Display header?</h3>
                <div id="header-state" class="toggles switch switch-blue">
                <?php
                                    foreach ( templateFactory::displayTemplatesHeader() as $key=>$value){
                ?>

                                        <input type="radio" class="switch-input" name="udel_theme_module_option"  id="<?php echo esc_attr( $value['id'] ) ?>"  value="<?php echo esc_attr( $value['id'] ) ?>"  <?php echo (  $udel_theme_module_option == $value['id'] ) ? 'checked' : ''?>><label for="<?php echo $value['id']; ?>" class="switch-label <?php echo (  $udel_theme_module_option == $value['id'] ) ? 'switch-label-on' : 'switch-label-off'?>"><?php echo $value['display']; ?></label>
                <?php
                                    }//display_templates_custom_header
                ?>

                <span class="switch-selection"></span>
                </div>
                  <h3>Custom college headers</h3>
                  <select name="udt_custom_header_value">
                  <?php

                                  foreach ( templateFactory::displayCustomTemplatesHeader() as $key=>$value){
          ?>

                 <option value="<?php echo esc_attr( $key ); ?>"<?php if (  $udt_custom_header_value == $key ){ $udt_custom_header_text = $value;
                                              ?>
                                       selected
                  <?php  }  ?>><?php echo esc_attr( $value ); ?></option>
                      <?php

                               }


              ?>
                </select>
                <input type="hidden" id="udt_custom_header_text" name="udt_custom_header_text" value="<?php echo esc_attr( $udt_custom_header_text ) ?>">
                </div>

<input type="hidden" name="" value="<?php echo $udt_custom_header_text ?>">
          <p class="submit"><input type="submit" class="button-primary" value="Save Changes"></p>
      </div>

      <div id="tabs-left-2" data-tab="1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
      <h2><?php echo $menu_text_foot ?></h2>
      <!-- footer main content -->
       <div class="tab-header twelve columns">

                <h3>Display footer?</h3>
                <div id="footer-state"  class="toggles switch switch-blue">
                <?php
                                    foreach ( templateFactory::displayTemplatesFooter() as $key=>$value){
                ?>

                                        <input type="radio" class="switch-input" name="udel_footer" id="<?php echo $value['id']; ?>"  value="<?php echo $value['id']; ?>"  <?php echo (  $udel_footer == $value['id'] ) ? 'checked' : ''?>><label for="<?php echo $value['id']; ?>" class="switch-label <?php echo (  $udel_footer == $value['id'] ) ? 'switch-label-on' : 'switch-label-off'?>"><?php echo $value['display']; ?></label>
                <?php
                                    }
                ?>
 <p></p>
                <span class="switch-selection"></span>

                </div>

            </div>
            <div id="footer-color" class="tab-header twelve columns">
                <h3>Text and logo color: <small class="hidden">Choose text and logo color.</small></h3>
                <div class="colors">
<?php
                    foreach ( templateFactory::colorTemplatesFooter() as $key=>$value){
?>
                     <label for="<?php echo $value['id']; ?>">
                        <input type="radio" name="udel_footer_color_option" id="<?php echo $value['id']; ?>" value="<?php echo $value['color']; ?>" <?php echo (  $udel_footer_color_option == $value['color'] ) ? 'checked' : ''?>>
                        <div class="<?php echo $value['class']; ?>"></div>
                    </label>
<?php
                    }
?>
                </div>
          </div>
          <p class="submit"><input type="submit" class="button-primary" value="Save Changes"></p>
        </div>
    </div>
    </div>
  </form>
</div>
    </div>
  <?php
}


function __construct(){
    // register actions
    add_action('admin_init', array(&$this, 'udtheme_register_settings'));
}

function udtheme_register_settings() {
  register_setting( 'udtheme', 'udel_theme_module_option' );
  register_setting( 'udtheme', 'udt_custom_header_value' );
  register_setting( 'udtheme', 'udt_custom_header_text' );
  register_setting( 'udtheme', 'udel_footer' );
  register_setting( 'udtheme', 'udel_footer_color_option' );
}


//add_action('admin_init', 'udtheme_register_settings');
add_action('admin_menu', 'udtheme_admin_sidebar');
add_action('wp_head','udtheme_header');
add_action('login_head', 'udtheme_header');
add_action('wp_footer','udtheme_footer');
add_filter('template_include','udtheme_include',1);
add_filter('shutdown','udtheme_shutdown',0);
//add_filter('all_plugins', 'modify_sitewide_plugins');

?>

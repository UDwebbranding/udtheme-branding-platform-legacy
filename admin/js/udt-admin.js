/**
    * jQuery no conflict function so you don't have to replace $ with jQuery
    * http://www.paulund.co.uk/jquery-in-wordpress-admin-area
    * https://digwp.com/2011/09/using-instead-of-jquery-in-wordpress/
*/
$=jQuery.noConflict();

jQuery(document).ready(function($){
    var row_tabs = $('row.tabs');

    var headerText = 'Header';
    var groupInput = $('.grey-main input[type="radio"]');
    var hdstateinput = $('#header-state > input');
    var notify = $('p.notify');
    var hdblank = $('#blankHeader');
    var hdblock = $('#blockHeader');

    var footerText = 'Footer';
    var ftblank = $('#blankFooter');
    var ftblock = $('#blockFooter');
    var ftcolorh3 = $('#footer-color h3 ');
    var ftcolorsmall = $('#footer-color small');
    var ftcolor = $('#footer-color');
    var ftstateinput = $('#footer-state > input');
    var ftcolorinput =$('input[name="udel_footer_color_option"]');

    var subbut = $('#ud_form input[type="submit"]');
    var Off = 'off';
    var On = 'on';

$(function () {
    var data = localStorage.getItem("setColorDiv");

    if (data !== null) {
        ftcolorinput.prop("disabled", true);
    }


});




    var currentTabIndex = "0";
    var tab = $('.row.tabs').tabs({
        active: localStorage.getItem("currentTabIndex"),
        activate: function(event, ui) {
            localStorage.setItem("currentTabIndex", ui.newPanel[0].dataset["tabIndex"]);
        }
    });

// function flash() {
//   $( "div" ).show().fadeOut( "slow" );
// }



// hdblank.click(function() {
//   $('.notify_header_off')
//     .on( "click", "notify", flash )
//     .toggleClass('red ')
//       .text('Header  is now hidden. Click "Save Changes" to take affect.');
// });

// hdblock.click(function() {
//   $('.notify_header')
//     .on( "click", "notify_header", flash )
//     .toggleClass('green ')
//       .text('Header is now visible. Click "Save Changes" to take affect.');
// });



 // hdblock.one("click", function() {
 //        notify
 //        .text('Header  is now showing. Click "Save Changes" to take affect.')
 //        .toggleClass('green')
 //        .removeClass('red');
 //    });

 //  hdblank.one("click", function() {
 //        notify
 //        .text('Header  is now showing. Click "Save Changes" to take affect.')
 //        .toggleClass('red')
 //        .removeClass('green');
 //    });

hdstateinput.on('change',function(){
        if(hdblank.prop('checked')){
            notify
                .text('You have turned ' + Off + ' the UD '+ headerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
                .addClass('red')
                .removeClass('green slideup');
            setTimeout(function(){
                notify.addClass('slideup');
            }, 3000);
            localStorage.removeItem('setHeader');
        }
        else {
            // hdblock.attr('data-state', 1);
            // hdblank.attr('data-state', null);
            notify
           .text('You have turned ' + On + ' the UD '+ headerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
                .addClass('green')
                .removeClass('red slideup');
            setTimeout(function(){
                notify.addClass('slideup');
            }, 3000);
            localStorage.setItem('setHeader', 1);
        }
    });

    ftstateinput.on('change', function () {
        if( ( ftblank ).prop('checked') ){
            ftcolorinput.prop('checked', false).prop('disabled',true).addClass('disabled');
          notify
                .text('You have turned ' + Off + ' the UD '+ footerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
                .addClass('red')
                .removeClass('green slideup');
                       setTimeout(function(){
                           notify.addClass('slideup');
                       }, 3000);
            localStorage.removeItem('setFooter');
            localStorage.setItem("setColorDiv", ftcolorinput.val());
        }
        else {
                var ftError = '';
                if (!ftcolorinput.is(':checked') ){
                                    ftError = 'Choose "Text and logo color"';
                }
                 ftcolorinput.prop('disabled',false).removeClass('disabled');
                 checkColor();

           notify
               .text('You have turned ' + On + ' the UD '+ footerText.toLowerCase() + '. ' + ftError + '. Click Save Changes for the setting to take effect.')
               .addClass('green')
               .removeClass('red slideup');
                      setTimeout(function(){
                          notify.addClass('slideup');
                      }, 3000);
                       localStorage.removeItem('setColorDiv');
            localStorage.setItem('setFooter', 1);
        }
    });


function checkColor() {
    ftcolorinput.on('click', function() {
        ftError = '';
    });
    return true;
}
//     groupInput.on('change',function(){
// // var checkChange = this.checked ? 'off' : 'on';
// //     notify.text('You have turned ' + checkChange + ' the UD header. Click Save Changes for the setting to take effect.' );


//         if(hdblank.prop('checked')){
//             notify
//                 .text('You have turned ' + Off + ' the UD '+ headerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
//                 .addClass('red')
//                 .removeClass('green slideup');
//             setTimeout(function(){
//                 notify.addClass('slideup');
//             }, 3000);
//             localStorage.removeItem('setHeader');
//         }
//         if(hdblock.prop('checked')){
//             notify
//                 .text('You have turned ' + On + ' the UD '+ headerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
//                 .addClass('green')
//                 .removeClass('red slideup')
//             setTimeout(function(){
//                 notify.addClass('slideup');
//             }, 3000);
//             localStorage.setItem('setHeader', 1);
//         }
//         if(ftblank.prop('checked')){
//             //ftcolor.prop('checked', false).prop('disabled',true).addClass('disabled');
//             notify
//                  .text('You have turned ' + Off + ' the UD '+ footerText.toLowerCase() + '. Click Save Changes for the setting to take effect.')
//                 .addClass('red')
//                 .removeClass('green slideup');
//             setTimeout(function(){
//                 notify.addClass('slideup');
//             }, 3000);

//             localStorage.removeItem('setFooter');
//         }
//          if(ftblock.prop('checked')){
//           //  ftcolor.prop('disabled',false).removeClass('disabled');
//            // var ftcolorCheck = ftcolor.checked ? 'Choose "Text and logo color' : '';
//             notify
//                 .text('You have turned ' + On + ' the UD '+ footerText.toLowerCase() +  '. Click Save Changes for the setting to take effect.')
//                 .addClass('green')
//                 .removeClass('red slideup')
//             setTimeout(function(){
//                 notify.addClass('slideup');
//             }, 3000);
//             localStorage.setItem('setFooter', 1);
//         }
//     });

});  // end document.ready

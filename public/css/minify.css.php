<?php
/**
 * https://ikreativ.com/combine-minify-css-with-php/
 * http://stackoverflow.com/questions/9862904/css-merging-with-php
 */
header('Content-type: text/css');
  ob_start("compress");
  function compress($buffer) {
      /* remove comments */
      $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
      /* remove tabs, spaces, newlines, etc. */
      $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
      return $buffer;
  }

  /* your css files */
  include('reset.css');
  include('header.css');
  include('footer.css');
  ob_end_flush();

/**
    * jQuery no conflict function so you don't have to replace $ with jQuery
    * http://www.paulund.co.uk/jquery-in-wordpress-admin-area
    * https://digwp.com/2011/09/using-instead-of-jquery-in-wordpress/
*/
$=jQuery.noConflict();

jQuery(document).ready(function($){
    var row_tabs = $('row.tabs');


    var hdstateinput = $('#header-state > input');
    var hdstate_p = $('#header-state p');
    var hdblank = $('#blankHeader');
    var hdblock = $('#blockHeader');

    var ftdivinput= $('#tabs-left-2 input');
    var ftstate_p = $('#footer-state p');
    var ftblank = $('#blankFooter');
    var ftblock = $('#blockFooter');
    var ftcolorh3 = $('#footer-color h3 ');
    var ftcolorsmall = $('#footer-color small');
    var ftcolor = $('input[name="udel_footer_color_option"]');

    var subbut = $('#ud_form input[type="submit"]');

    var fadeout = ".fadeOut('fast')";



var currentTabIndex = "0";
/**
 * http://stackoverflow.com/questions/14313270/jquery-ui-tabs-no-longer-supporting-cookie-now-what
 * jquery ui tabs no longer supporting cookie? now what?
 */
    var tab = $('.row.tabs').tabs({
         active: localStorage.getItem("currentTabIndex"),
    activate: function(event, ui) {
        localStorage.setItem("currentTabIndex", ui.newPanel[0].dataset["tabIndex"]);
    }
    });

/**
     * http://jsfiddle.net/finstah/bufk76wo/
     * Save checkbox state with localStorage

    * Save header checkbox state with localStorage
    * http://jsfiddle.net/finstah/bufk76wo/
*/
    hdstateinput.on('click', function () {
        if( ( hdblank ).prop('checked') ){
            hdstate_p.text('Header is off.').addClass('red').removeClass('green').fadeIn('fast', function() {
                $(this).delay(2000)[fadeout];
            });
            localStorage.removeItem('setHeader');
        }
        else {
            hdstate_p.text('Header is on.').addClass('green').removeClass('red').fadeIn('fast', function() {
                $(this).delay(2000)[fadeout];
            });
            localStorage.setItem('setHeader', 1);
        }
    });

/**
     * http://jsfiddle.net/finstah/bufk76wo/
     * Save checkbox state with localStorage

    * Save footer checkbox state with localStorage
    * http://jsfiddle.net/finstah/bufk76wo/
*/
    ftdivinput.on('click', function () {
        if( ( ftblank ).prop('checked') ){
            ftcolor.prop('checked', false).prop('disabled',true).addClass('disabled');
            ftcolorsmall[fadeout];
            ftstate_p.text('Footer is off.').addClass('red').removeClass('green').fadeIn('fast', function() {
                $(this).delay(2000)[fadeout];
            });
            localStorage.removeItem('setFooter');
        }
        else {
            ftcolor.prop('disabled',false).removeClass('disabled');
            ftstate_p.text('Footer is on.').addClass('green').removeClass('red').fadeIn('fast', function() {
            $(this).delay(2000)[fadeout];
            });
            if ( !ftcolor.is(':checked') ){
               ftcolorsmall.removeClass('hidden').fadeIn('fast');
                subbut.prop('disabled', true);
            }
            else {
                ftcolorsmall.addClass('hidden')[fadeout];
                subbut.prop('disabled', false);
            }
            localStorage.setItem('setFooter', 1);
        }
    });
});  // end document.ready
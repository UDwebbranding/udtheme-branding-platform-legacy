<?php
/**
 * templateInterface
 * The bare minimum methods required to be implemented to be a template
*/

interface templateInterface{
  function html_ud_css();
  function html_ud_js();
  function html_ud_header();
  function html_ud_footer();
}
?>

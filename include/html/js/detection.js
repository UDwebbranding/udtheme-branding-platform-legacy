/**
    * Feature detection for CSS Transitions, SVG and Touch Events support
    * Javscript Simple Feature Detection
    * http://niklaspostulart.de/2015/09/js-simple-detection
*/
window.Detection = (function(window, document) {
  var Detection, className, detectSVG, detectCSSTrans, detectTouch, detectIE10, html;

  html = document.documentElement;

  className = html.className.replace("no-js", "js");

  detectSVG = function() {
    return !!document.createElement("svg").getAttributeNS;
  };
    detectCSSTrans = function() {
    return window.TransitionEvent;
  };
   detectTouch = function() {
    return  (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
  };
  detectIE10 = function() {
    return document.all;
  };

  Detection = {
    svg : detectSVG(),
    transition : detectCSSTrans(),
    touch : detectTouch(),
    ie10 : detectIE10()
  };

if (Detection.svg) {
    className += ' svg ';
}
else {
    className += ' no-svg ';
}
if (Detection.transition) {
    className += ' csstransitions ';
}
else {
    className += ' no-csstransitions ';
}
if (Detection.touch) {
    className += ' touchevents ';
}
else {
    className += ' no-touchevents ';
}
if (Detection.ie10) {
    className += ' ie10 ';
}
// add class names and trim white space after last class.
  html.className = className.replace(/\s*$/,"");

  return Detection;
})(window, document);

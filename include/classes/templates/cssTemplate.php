<?php

class cssTemplate
    extends baseTemplate{

  public function __construct($dept_in){
    parent::__construct($dept_in);
   $this->setUdThemeDir(MakeItSoNo1::$htmlDir);
   $this->setUdCssDir(MakeItSoNo1::$cssDir);
  }

  public function html_ud_css(){
?>
 <link rel="stylesheet" type="text/css" href="<?php echo $this->getUdCssDir() ; ?>/minify.css.php?ver=1.0">

<?php
  } // end html_ud_css
} // end cssTemplate
?>

<?php

class blank
	extends baseTemplate{

  public function __construct($title_in, $dept_in, $keywords_in=null, $description_in=null){
    parent::__construct($title_in, $dept_in, $keywords_in=null, $description_in=null);
    $this->setUdThemeDir(MakeItSoNo1::$htmlDir);
    $this->setUdCssDir(MakeItSoNo1::$cssDir);
    $this->setUdBannerImg("");
  }

  public function html_ud_css(){
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getUdCssDir() ; ?>/header.min.css?ver=4.0">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getUdCssDir() ; ?>/footer.min.css?ver=4.0">
<?php
    if ( get_option( 'udel_theme_module_option' ) == 'blankHeader') :
     $this->setUdHeaderCSS("");
    endif;
  }
  public function html_ud_js(){ return ""; }
  public function html_user_js() {return ""; }
  public function html_ud_header(){return "";}
}
?>
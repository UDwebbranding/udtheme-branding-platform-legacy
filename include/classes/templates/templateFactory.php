<?php
/**
   * templateFactory
   * Creates content on plugin admin screen.
*/

class templateFactory{
  public static function mainTitleOptions(){
    $title_template = array(
        array(
          "id" => "Header Options",
          "hash"    => "#head_content"
        ),
        array(
          "id" => "Footer Options",
          "hash"    => "#foot_content"
        )
      );
    return $title_template;
  }

  public static function displayTemplatesHeader(){
    $display_templates_header = array(
        array(
           "id" => "blankHeader",
            "display"       => "No"
        ),
          array(
           "id" => "blockHeader",
            "display"       => "Yes"
        ),
      );
    return $display_templates_header;
  }

  public static function displayCustomTemplatesHeader(){
    $display_templates_custom_header =
      array(
        NULL => 'Default (No College)',
        'lerner'  => 'Alfred Lerner College of Business &amp; Economics',
        'canr'    => 'College of Agriculture &amp; Natural Resources',
        'cas'     => 'College of Arts &amp; Sciences',
        'ceoe'    => 'College of Earth, Ocean, &amp; Environment',
        'cehd'    => 'College of Education &amp; Human Development',
        'engr'    => 'College of Engineering',
        'chs'     => 'College of Health Sciences',
      );

    // $display_templates_custom_header =
    //       array(
    //             "option" => "Default (No College)",
    //             "url"    => null
    //       ),
    //       array(
    //             "option" => "Alfred Lerner College of Business &amp; Economics",
    //             "url"    => "lerner"
    //       ),
    //       array(
    //             "option" => "College of Agriculture &amp; Natural Resources",
    //             "url"    => "canr"
    //       ),
    //       array(
    //             "option" => "College of Arts &amp; Sciences",
    //             "url"    => "cas"
    //       ),
    //       array(
    //             "option" => "College of Earth, Ocean, &amp; Environment",
    //             "url"    => "ceoe"
    //       ),
    //       array(
    //             "option" => "College of Education &amp; Human Development",
    //             "url"    => "cehd"
    //       ),
    //       array(
    //             "option" => "College of Engineering",
    //             "url"    => "engr"
    //       ),
    //       array(
    //             "option" => "College of Health Sciences",
    //             "url"    => "chs"
    //   );
    return $display_templates_custom_header;
  }
  public static function displayTemplatesFooter(){
    $display_templates_footer = array(
        array(
           "id" => "blankFooter",
            "display"       => "No"
        ),
          array(
           "id" => "blockFooter",
            "display"       => "Yes"
        ),
      );
    return $display_templates_footer;
  }

  public static function colorTemplatesFooter(){
    $color_templates_footer = array(
        array(
                  "color" => "White",
                  "id"        => "rad_white_footer",
                  "class"   => "scheme_white"

                ),
        array(
          "color" => "Blue",
          "id"        => "rad_blue_footer",
          "class"   => "scheme_blue"
        ),
      );
      return $color_templates_footer;
    }
   public static function getTemplate( $template){
    foreach ( templateFactory::displayTemplatesHeader() as $key=>$value){
      switch( $value ){
        case "blankHeader":
        return new blank( $dept_in );
        break;

        default:
        return new cssTemplate( null );
        break;
      }
    }
  }
}
?>

<?php

class sealBlueHeaderFluid
    extends baseTemplate{

  public function __construct($dept_in){
    parent::__construct($dept_in);
   $this->setUdThemeDir(MakeItSoNo1::$htmlDir);
   $this->setUdCssDir(MakeItSoNo1::$cssDir);
  }

  public function html_ud_css(){
?>
<?php
     if ( get_option( 'udel_theme_module_option' ) != 'blankHeader'):
?>
 <link rel="stylesheet" type="text/css" href="<?php echo $this->getUdCssDir() ; ?>/header.min.css?ver=3.1">
 <?php
    endif;
?>

<?php
    if ( get_option('udel_footer') != 'blankFooter' ):
?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->getUdCssDir() ; ?>/footer.css?ver=3.1">
<?php
    endif;
?>

<?php
  } // end html_ud_css
} // end class edu_udel_template_sealBlueHeaderFluid
?>
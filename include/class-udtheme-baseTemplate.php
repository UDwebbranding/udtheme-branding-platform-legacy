<?php
/**
 * baseTemplate
 * The standard template the most other templates will extend
 * Use this as the basis for a template unless you are doing something
 *  radically different.
 * http://codex.wordpress.org/Function_Reference/is_plugin_active
 * Detect plugin. For use on Front End only.
 * http://grinninggecko.com/2010/12/31/activating-deactivating-wordpress-plugins-programmatically/
 * Activating and Deactivating WordPress Plugins Programmatically
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
// check for plugin using plugin name
if ( is_plugin_active( 'ud-branding/ud-branding.php' ) ):
  deactivate_plugins('ud-branding/ud-branding.php');
endif;


abstract class baseTemplate
    implements templateInterface{

  private $udThemeServerUrl;  //example: https://sites.udel.edu
  private $udThemeDir;    //example: /include/themes
  private $UdCssDir;
  private $UdhtmlDir;
  private $css;
  private $js;
  private $udBannerImg;
  private $udFooterImg;
  private $udFooterCSS;
  private $udHeaderCSS;
  private $udtUrl;

  public function __construct( $dept_in ) {
    $this->udThemeServer = "";
    $this->dept = $dept_in;
   // $this->setUdtUrl(MakeItSoNo1::$htmlDir);
  }



  public function html_ud_head_content(){
    $html_out .= $this->html_ud_css();
    $html_out .= $this->html_ud_js();
    return $html_out;
  }

  public function html_ud_js(){}
  public function html_ud_footer_js(){}
  public function html_ud_header(){

   // $html_out .= '<div id="jsWarning"><div>Javascript is required on this website for the best possible user experience. </div></div>'. "\n";
        $udel_header = get_option('udel_theme_module_option');





        $udel_header_custom_text = get_option('udt_custom_header_text');//$display_templates_custom_header["custom_attr"]["url"];


        $udel_header_custom_url = get_option('udt_custom_header_value');


         if ( $udel_header && $udel_header != 'blankHeader' && !is_plugin_active( 'ud-branding/ud-branding.php' ) ):



          $html_out .= 'htmldir' .$htmlDir.'<header role="banner" id="ud-header" class="flex-container flex-center">' . "\n";
          $html_out .= '<div>'. "\n";
          $html_out .= '<a title="University of Delaware" href="http://www.udel.edu/" id="udbrand_logo">'. "\n";
          $html_out .= '<img alt="University of Delaware" id="ud_primary_logo_big" width="218" height="91" src=" ' $htmlDir .'/img/logo-udel.png">'. "\n";
          $html_out .= '</a>'. "\n";
          $html_out .= '<span><a id="college_global_headerTitle" href="http://www.'.$udel_header_custom_url.'.udel.edu/">'.$udel_header_custom_text.'</a></span>'. "\n";
          $html_out .= '</div>'. "\n";
          $html_out .= '</header>' . "\n";

          return $html_out;

        endif;
  } // end html_ud_header

  public function html_ud_footer( ){
    $udel_footer = get_option('udel_footer');
    if ( $udel_footer && $udel_footer != 'blankFooter' && !is_plugin_active( 'ud-branding/ud-branding.php' ) ) :
  ?>
<footer class="flex-container" itemtype="http://schema.org/EducationalOrganization" role="contentinfo" itemscope="" id="udbrand_footer">
 <div itemtype="http://schema.org/ImageObject" itemscope="" id="ftlogo">
  <a href="http://www.udel.edu">
  <img src="<?php echo $this->getUdThemeUrl();?>/public/img/circle-ud-<?php if ( get_option('udel_footer_color_option') == 'Blue' ):
                echo  'blue.png';
              else:
                echo  'white.png';
              endif;
              ?>" alt="University of Delaware" width="350" height="130">
</a>
</div>
<div>
  <ul id="sociallinks">
<?php
   function template_footer_social() {
        $footlinks = array(
            "twitter" => array(
                "url"   => "https://twitter.com/UDelaware",
                "icon" => "twitter"
            ),
           "facebook" => array(
                 "url"   => "https://www.facebook.com/udelaware",
                 "icon" => "facebook"
             ),
           "instagram" => array(
                "url"   => "https://www.instagram.com/udelaware",
                "icon" => "instagram"
            ),
           "youtube" => array(
                 "url"   => "https://www.youtube.com/univdelaware",
                 "icon" => "youtube"
             ),
           "pintrest" => array(
                "url"   => "https://www.pinterest.com/udelaware/",
                "icon" => "pintrest"
            ),
           "linkedin" => array(
                 "url"   => "https://www.linkedin.com/edu/school?id=18070",
                 "icon" => "linkedin"
             )
        ); // end $footlinks


        function buildLinks($footlinks) {

            foreach($footlinks as $key => $value) {
?>
<li><a aria-label="<?php echo $key ?>" href="<?php echo $value['url'] ?>"><span data-icon="<?php echo $value['icon'] ?>"></span></a></li>
<?php
            }
?>
    </ul>
    </div>
    <hr id="yellowbar">
    <div id="ud-legal">
      <ul class="small">
      <li>&copy; <?php echo Date('Y'); ?> University of Delaware</li>
        <li><a href="https://www.udel.edu/home/comments.html" target="_blank">Comments</a></li>
        <li><a href="https://www.udel.edu/home/legal-notices.html" target="_blank">Legal Notices</a></li>
      </ul>
    </div>
</footer>
<?php
        } // close buildLinks
        buildLinks($footlinks);
    } // close template_footer_social
return template_footer_social();
 return html_ud_footer_js();

        endif;
        } // close html_ud_footer


  public function getUdThemeServerUrl() { return $this->udThemeServerUrl; }
  public function getUdThemeDir() { return $this->udThemeDir; }
  public function getUdCssDir() { return $this->UdCssDir; }
  public function getHtmlDir() { return $this->UdhtmlDir; }
  public function getUdThemeUrl() { return $this->udThemeServerUrl . $this->udThemeDir; }
  public function getTitle() { return $this->title; }
  public function getCss() { return $this->css; }
  public function getJs() { return $this->js; }
  public function getUdFooter() { return $this->udel_footer; }
  public function getSearchSiteUrl() { return $this->searchSiteUrl; }
  public function getUdBannerImg() { return $this->udBannerImg; }
  public function getUdFooterImg() { return $this->udFooterImg; }
  public function getUdFooterCSS() { return $this->udFooterCSS; }
  public function getUdHeaderCSS() { return $this->udHeaderCSS; }
  public function setUdThemeServer( $ts ) { $this->udThemeServer = $ts; }
  public function setUdThemeDir( $td ) { $this->udThemeDir = $td; }
  public function setUdCssDir( $cssd ) { $this->UdCssDir = $cssd; }
  public function setHtmlDir($htmld) { $this->UdhtmlDir = $htmld; }
  public function setCss($css_arr) { $this->css = $css_arr; }
  public function addCss($css_in)  { $this->css[] = $css_in; }
  public function setJs($js_arr) { $this->js = $js_arr; }
  public function addJs($js_in)  { $this->js[] = $js_in; }
  public function setUdBannerImg($img_in) { $this->udBannerImg = $img_in; }
  public function setUdFooterImg($img_in) { $this->udFooterImg = $img_in; }
  public function setUdFooterCSS($css_in) { $this->udFooterCSS = $css_in; }
  public function setUdHeaderCSS($css_in) { $this->udHeaderCSS = $css_in; }

  public function getUdtUrl() { return $this->udtUrl; }
  public function setUdtUrl($udtUrl_in) { $this->udtUrl = $udtUrl_in; }
}
?>

<?php
/*
  *  constants
*/
class MakeItSoNo1{
  public static $timezone;
  public static $wwwRootDir;
  public static $constantsDir;
  public static $htmlDir;
  public static $cssDir;
  public static $imgDir;
  public static $jsDir;
  public static $incDir;
  public static $classDir;
  public static $baseUrl;
  public static $udTheme;
  public static $errors;

  public static function loadConfig( $config=null ){
    // Defaults can be overriden with $config array
    self::$timezone = "America/New_York";
    self::$wwwRootDir =  realpath($_SERVER['DOCUMENT_ROOT']);
    self::$constantsDir = __DIR__;
    self::$classDir = self::$constantsDir . "/classes/templates";
    self::$baseUrl = plugins_url() . "/udtheme" ;
    self::$htmlDir = self::$baseUrl . "/public";
    self::$cssDir = self::$htmlDir . "/css";
    self::$imgDir = self::$htmlDir . "/img";
    self::$jsDir = self::$htmlDir . "/js";
    self::$udTheme = "UDTheme";
    self::$errors = null;

    if ( $config != null ){
      if ( isset($config['timezone']) ){
        self::$timezone = $config['timezone'];
      }
      if ( isset($config['udTheme']) ){
        self::$udTheme = $config['udTheme'];
      }
      // if ( isset($config['baseUrl'])){
      //   self::$baseUrl = $config['baseUrl'];
      //   self::$htmlDir = self::$baseUrl . "public";
      //   self::$cssDir = self::$htmlDir . "/css";
      //   self::$jsDir = self::$htmlDir . "/img";
      //   self::$jsDir = self::$htmlDir . "/js";

      // }
      // if ( isset($config['htmlDir'] )){
      //   self::$htmlDir = $config['htmlDir'];
      // }
    }
  }
}

MakeItSoNo1::loadConfig();
date_default_timezone_set(MakeItSoNo1::$timezone);

function udtheme_server_autoloader($class_name) {

    if (!isset($class_name) || is_null($class_name) || $class_name == "") {
      return false;
    }

    $path = str_replace('_', '/', $class_name);

    // The application include path first
    if (file_exists(MakeItSoNo1::$classDir."/".$path.".php") && include_once(MakeItSoNo1::$classDir."/".$path.".php")) {
        return true;
    }
//    return false;
}
spl_autoload_register("udtheme_server_autoloader");
?>
